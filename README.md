# HEOS (Denon) Audio/Video Amplifier Remote Control (Web UI)

This Heos Remote Control is a web UI allowing you to remotely
control your Denon AV amplifier on the same LAN through its telnet
interface.

This requires a back-end (currently in development) to be functional.

## Demo and Screenshots

You can try the UI at https://jiehong.gitlab.io/heos_remote_control/ (not
connected to any back-end).

Screenshots: TODO

## Local Dev

You can try this UI with (assuming you have [simple-http-server](https://github.com/TheWaWaR/simple-http-server) installed):

```sh
$ simple-http-server -i
```

And then head over to http://0.0.0.0:8000
